var interval = 15;
var timezone = "America/New_York";
var theme = "dark";

jQuery(document).ready(function () {
    var urlsymbols = getUrlSymbols();
    if (urlsymbols) symbols = urlsymbols;

    var tv_defaults = {
        "autosize": true,
        "interval": interval,
        "timezone": timezone,
        "theme": theme,
        "style": "1",
        "locale": "en",
        "toolbar_bg": "#f1f3f6",
        "enable_publishing": false,
        "hide_top_toolbar": true,
        "withdateranges": true,
        "whitelabel": true,
    }

    var row = getNewRow();
    var tv_charts = [];
    symbols.forEach(function (value, i) {
        if (i > 0 && i % 2 == 0) {
            // Time for a new row
            appendToContainer(row);
            row = getNewRow();
        }

        var column = document.createElement('div');
        column.classList.add('col-6');

        var chart_div_id = 'tv_' + i;
        var chart = document.createElement('div');
        chart.setAttribute('id', chart_div_id);
        column.appendChild(chart);

        row.appendChild(column);

        tv_charts.push(Object.assign({}, tv_defaults, {
            "symbol": value,
            "container_id": chart_div_id,
        }));
    });

    // Append final row
    appendToContainer(row);

    tv_charts.forEach(function (chart) {
        new TradingView.widget(chart);
    })

    var noSleep = new NoSleep();
    // Enable wake lock.
    // Uses NoSleep https://github.com/richtr/NoSleep.js/
    document.addEventListener('click', function enableNoSleep() {
        document.removeEventListener('click', enableNoSleep, false);
        noSleep.enable();
    }, false);

    // Reenable NoSleep if the PWA goes to sleep
    document.addEventListener('visibilitychange', () => {
        if ( document.visibilityState == "visible" ) {
            noSleep.enable(); // keep the screen on!
        }
    })
});

function getNewRow() {
    let row = document.createElement('row');
    row.classList.add('row');
    return row;
}

function appendToContainer(row, container = null) {
    if (!container) {
        container = document.getElementsByClassName('container-fluid')[0];
    }
    return container.appendChild(row);
}

function getUrlSymbols() {
    let urlparams = new URLSearchParams(window.location.search);
    let symbols = urlparams.get('symbols');
    if (!symbols) return null;
    return symbols.split(',');
}
